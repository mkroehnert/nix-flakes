{
  description = "nix-flakes";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    let
      # overlay function can be put into dedicated file
      localOverlay = final: prev: {
        alglib = prev.callPackage ./alglib/alglib.nix { };
        hdrmerge = prev.callPackage ./hdrmerge/hdrmerge.nix { };
        #devShell = prev.callPackage ./shell.nix { };
      };

      pkgsForSystem = system: import nixpkgs {
        overlays = [
          localOverlay
        ];
        inherit system;
      };
    in flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = pkgsForSystem system;
      in {
        packages = flake-utils.lib.flattenTree {
          inherit (pkgs)
            #devShell
            alglib
            hdrmerge;
        };

        defaultPackage = pkgs.hdrmerge;
    }) // {
      overlay = localOverlay;
    };

  nixConfig.bash-prompt = "\[nix-flakes\]$ ";
}

