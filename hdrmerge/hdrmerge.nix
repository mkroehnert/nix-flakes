{ lib
, stdenv, fetchFromGitHub
, cmake , extra-cmake-modules
, qt48Full
, libraw
, exiv2
, zlib
, alglib
, pkgconfig
, makeDesktopItem
}:

stdenv.mkDerivation rec {
  name = "hdrmerge";

  src = fetchFromGitHub {
    owner = "jcelaya";
    repo = "hdrmerge";
    rev = "v0.5.0";
    sha256 = "sha256:1lal2r6qm8h9igd5976ral1g7dfp6mmy6jn0wynq36g25mdz4cai";
  };

  # dev dependencies
  nativeBuildInputs = [
    cmake
    pkgconfig
  ];

  # runtime dependencies
  buildInputs = [ qt48Full libraw exiv2 zlib alglib ];

  cmakeFlags = [
    "-DALGLIB_DIR:PATH=${alglib}"
  ];

  patches = [
    # patch FindAlglib.cmake to respect ALGLIB_DIR
    # take latest FinxExiv2.cmake from v0.6 release branch
    # fix DECODER compile error
    #   https://github.com/jcelaya/hdrmerge/commit/413bccd9f38594a5fca0b0a1fb01c23d4fda6843
    ./patch-hdrmerge-CMake.patch
  ];

  desktopItem = makeDesktopItem {
    name = "HDRMerge";
    exec = "@out@/bin/hdrmerge";
    icon = "hdrmerge";
    comment = meta.description;
    desktopName = "HDRMerge";
    genericName = "HDRMerge";
    categories = "Graphics;";
  };

  postInstallPhase =
    ''
      # Make a desktop item
      mkdir -p $out/share/icons/ $out/share/applications/
      cp images/logo.png $out/share/icons/hdrmerge.png
      cp ${desktopItem}/share/applications/* $out/share/applications/
    '';

  meta = {
    description = "HDRMerge fuses two or more raw images into a single raw with an extended dynamic range.";
    homepage = https://jcelaya.github.io/hdrmerge/;
    license = lib.licenses.gpl3Plus;

    longDescription = ''
HDRMerge fuses two or more raw images into a single raw with an extended dynamic range. It can import any raw image supported by LibRaw, and outputs a DNG 1.4 image with floating point data. The output raw is built from the less noisy pixels of the input, so that shadows maintain as much detail as possible. This tools also offers a GUI to remove 'ghosts' from the resulting image.
    '';
  };
}
