# How To Use

Using this package requires a Flakes enabled nix commandline utility.

Flakes are currently in experimental status but instructions for enabling them can be found here https://nixos.wiki/wiki/Flakes#Installing_flakes

Build and run HDRMerge with the following commands (tested with Nix 2.4)

``` shell
$ cd <some directory>
  # either pass URL as quoted string, or depending on the shell use \? for escaping the questionmark
$ nix-flakes build "git+https://codeberg.org/mkroehnert/nix-flakes?ref=dev"
  # execute the program
$ ./result/bin/hdrmerge
```

Flakes commandline reference: https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-flake.html

# Development

clean build with sources copied from `/nix/store`

``` shell
$ # clone repo and cd into the checkout
$ nix develop
$ echo "src = $src" && cd $(mktemp -d) && unpackPhase && cd *
$ configurePhase
$ buildPhase
$ # other commands (e.g. installPhase)
```

# Credits

* HDRMerge and its developers: https://github.com/jcelaya/hdrmerge/
* Jon Ringer for his great nix flakes template project: https://github.com/jonringer/nix-template

